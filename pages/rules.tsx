import Link from "next/link"
import styles from "../styles/Home.module.css"

const rules = () => {
  return (
    <div className={styles.content}>
      <h1>Rules</h1>
      <div>
        <div>
          <Link href='./'>
            <button className={styles.button}>Return home</button>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default rules
