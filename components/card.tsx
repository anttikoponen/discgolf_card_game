import { CardType } from "../types/types";
import styles from "../styles/Card.module.css";

type Props = {
  card: CardType;
  onUse: (card: CardType) => void;
};

const Card: React.FC<Props> = ({ card, onUse }) => {
  const handleUseClick = () => {
    onUse(card);
  };

  return (
    <div className={styles.card}>
      <div className="card-details">
        <h3>{card.name}</h3>
        <h4 className={styles.usage}>{card.usage}</h4>
        <p>{card.description}</p>
        <button className={styles.button} onClick={handleUseClick}>
          Use Card
        </button>
      </div>
    </div>
  );
};

export default Card;
