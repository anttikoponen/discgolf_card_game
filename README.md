This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First install the project

`npm install`

---

## Database init with Docker, PostgeSQL and Prisma

Create .env file with database url

`DATABASE_URL="postgresql://postgres:postgres@localhost:5555/discgolf_game?schema=public"`

### Docker:

Oped Docker

Initialise docker container

`docker-compose -f docker-compose.development.yml up db -d`

Start docker container if not already started

`docker start postgres-dev`

### Prisma

Seed database

- `npx prisma generate`

- `npx prisma db push`

- `npx prisma db seed`

  ***

## Run the app

Run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

---
