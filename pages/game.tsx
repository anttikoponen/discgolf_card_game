import { useEffect, useState } from "react";
import Card from "../components/card";
import { getCards } from "../lib/prisma";
import { CardType } from "../types/types";
import { GetStaticProps, NextPage } from "next";
import styles from "../styles/Home.module.css";

type Props = {
  deck: CardType[];
};
const Game: NextPage<Props> = ({ deck }) => {
  const [cards, setCards] = useState<CardType[]>([]);
  const [hand, setHand] = useState<CardType[]>([]);
  const [usedCards, setUsedCards] = useState<CardType[]>([]);

  useEffect(() => {
    const fetchCards = async () => {
      const allCards = deck;
      const shuffledCards = allCards.sort(() => 0.5 - Math.random());
      const selectedCards = shuffledCards.slice(0, 5);
      setCards(shuffledCards);
      setHand(selectedCards);
      console.log("fetch");
    };
    fetchCards();
  }, [deck]);

  const drawCard = () => {
    const availableCards = cards.filter(
      (card) => !usedCards.includes(card) || !hand.includes(card)
    );
    const newCard =
      availableCards[Math.floor(Math.random() * availableCards.length)];
    setHand([...hand, newCard]);
  };

  const useCard = (usedCard: CardType) => {
    setHand(hand.filter((card) => card !== usedCard));
    if (!usedCards.includes(usedCard)) setUsedCards([...usedCards, usedCard]);
  };
  console.log("Used cards: ", usedCards);
  console.log("Hand cards: ", hand);
  return (
    <div>
      <nav className={styles.navbar}>Game Name</nav>
      <div className={styles.content}>
        {hand.map((card) => (
          <Card key={card.id} card={card} onUse={useCard} />
        ))}
      </div>
      <div className={styles.footer}>
        <button className={styles.draw} onClick={drawCard}>
          Draw a Card
        </button>
      </div>
    </div>
  );
};

export const getStaticProps: GetStaticProps<Props> = async () => {
  const deck = await getCards();

  return {
    props: {
      deck,
    },
  };
};

export default Game;
