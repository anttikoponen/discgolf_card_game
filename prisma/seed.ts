import { PrismaClient } from "@prisma/client"
import { deckOfCards } from "./seedData"

const prisma = new PrismaClient()
async function main() {
  for (let data of deckOfCards) {
    console.log(data)
    const item = await prisma.card.create({
      data,
    })
    console.log(item)
  }
}

main()
  .then(async () => {
    await prisma.$disconnect()
  })

  .catch(async e => {
    console.error(e)

    await prisma.$disconnect()

    process.exit(1)
  })
