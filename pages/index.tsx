import Head from "next/head";
import Image from "next/image";
import { Inter } from "@next/font/google";
import { getCards } from "../lib/prisma";
import Card from "../components/card";
import { CardType } from "../types/types";
import { GetServerSideProps, GetStaticProps, NextPage } from "next";
import Link from "next/link";
import styles from "../styles/Home.module.css";

const inter = Inter({ subsets: ["latin"] });

type Props = {
  cards: CardType[];
};

const IndexPage: NextPage<Props> = ({ cards }) => {
  return (
    <div>
      <div className={styles.main}>
        <h1 className={styles.center}>Discgolf Game</h1>
        <div className={styles.footer}>
          <Link href="/game">
            <button className={styles.button}>Start game</button>
          </Link>
          <Link href="/rules">
            <button className={styles.button}>Rules</button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default IndexPage;
