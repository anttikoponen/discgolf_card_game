export const deckOfCards = [
  {
    name: "Roll it!",
    usage: "Before shot",
    description:
      "Force an opponent to throw a roller on the upcoming drive or approach.",
    image: "profile.jpg",
  },
  {
    name: "Thumber",
    usage: "Before shot",
    description:
      "Force an opponent to throw a thumber on the upcoming drive or approach.",
    image: "profile.jpg",
  },
  {
    name: "Tomahawk",
    usage: "Before shot",
    description:
      "Force an opponent to throw a Tomahawk on the upcoming drive or approach.",
    image: "profile.jpg",
  },
  {
    name: "Sidearm",
    usage: "Before shot",
    description:
      "Force an opponent to throw a sidearm on the upcoming drive or approach",
    image: "profile.jpg",
  },
  {
    name: "Choose disc",
    usage: "Before shot",
    description:
      "Choose one of your opponent's discs. That disc must be used on his/her next shot.",
    image: "profile.jpg",
  },
  {
    name: "Choose Disc",
    usage: "Before shot",
    description:
      "Choose one of your opponent's discs. That disc must be used on his/her next shot. ",
    image: "profile.jpg",
  },
  {
    name: "Straddle putt",
    usage: "Before putt",
    description: "Force opponent to straddle putt on the upcoming putt. ",
    image: "profile.jpg",
  },
  {
    name: "Jump Putt",
    usage: "Before putt",
    description:
      "Force opponent to jump putt on the upcoming putt. (No C1 penalty applies)",
    image: "profile.jpg",
  },
  {
    name: "Fire in the Hole!",
    usage: "Before shot",
    description:
      "Force an opponent to throw a grenade on the upcoming drive or approach. ",
    image: "profile.jpg",
  },
  {
    name: "Fire in the Hole!",
    usage: "Before shot",
    description:
      "Force an opponent to throw a grenade on the upcoming drive or approach. ",
    image: "profile.jpg",
  },
  {
    name: "Mini Drive",
    usage: "Before shot",
    description:
      "Force an opponent to throw a mini for his/her upcoming drive. ",
    image: "profile.jpg",
  },
  {
    name: "Mini Drive",
    usage: "Before shot",
    description:
      "Force an opponent to throw a mini for his/her upcoming drive. ",
    image: "profile.jpg",
  },
  {
    name: "nnnnNunan!",
    usage: "After shot",
    description:
      "Heckle an opponent durring one of his/her shots. Reveal and discard this card after. ",
    image: "profile.jpg",
  },
  {
    name: "nnnnNunan!",
    usage: "After shot",
    description:
      "Heckle an opponent durring one of his/her shots. Reveal and discard this card after. ",
    image: "profile.jpg",
  },
  {
    name: "No Chains",
    usage: "Before tee",
    description:
      "Choose an opponent. That player may not play anymore cards untill he/she hits a putt without using the chains. ",
    image: "profile.jpg",
  },
  {
    name: "No Chains",
    usage: "Before tee",
    description:
      "Choose an opponent. That player may not play anymore cards untill he/she hits a putt without using the chains. ",
    image: "profile.jpg",
  },
  {
    name: "Zip It!",
    usage: "Before tee",
    description:
      "Force an opponent to play the upcoming hole without speaking. If he/she does there is a 1 stroke or 1 skin penalty. All other opponents may also play a card on that player this hole. ",
    image: "profile.jpg",
  },
  {
    name: "Zip It!",
    usage: "Before tee",
    description:
      "Force an opponent to play the upcoming hole without speaking. If he/she does there is a 1 stroke or 1 skin penalty. All other opponents may also play a card on that player this hole. ",
    image: "profile.jpg",
  },
  {
    name: "Too Many Cards",
    usage: "Before tee",
    description:
      "Force an opponent to discard 2 cards at random from his/her hand.",
    image: "profile.jpg",
  },
  {
    name: "Too Many Cards",
    usage: "Before tee",
    description:
      "Force an opponent to discard 2 cards at random from his/her hand. They can draw 1",
    image: "profile.jpg",
  },
  {
    name: "Change is Good",
    usage: "Before tee",
    description:
      "Force an opponent to discard his/her hand and draw the same number of cards from the deck.",
    image: "profile.jpg",
  },
  {
    name: "Change is Good",
    usage: "Before tee",
    description:
      "Force an opponent to discard his/her hand and draw the same number of cards from the deck.",
    image: "profile.jpg",
  },
  {
    name: "Unfimiliar Discs",
    usage: "Before shot",
    description:
      "Trade one player's disc for any other player's disc. Those discs must be used for the upcoming drive. ",
    image: "profile.jpg",
  },
  {
    name: "Unfimiliar Discs",
    usage: "Before shot",
    description:
      "Trade one player's disc for any other player's disc. Those discs must be used for the upcoming drive. ",
    image: "profile.jpg",
  },
  {
    name: "Bizarro Hole Solo",
    usage: "Before tee",
    description:
      "Force an opponent to drive with a putter and putt with a driver on the upcoming hole. ",
    image: "profile.jpg",
  },
  {
    name: "Bizarro Hole Solo",
    usage: "Before tee",
    description:
      "Force an opponent to drive with a putter and putt with a driver on the upcoming hole. ",
    image: "profile.jpg",
  },
  {
    name: "Cancel Shot",
    usage: "After shot",
    description:
      "Cancel any shot just taken by an opponent. That opponent must throw again using a different disc of his/her choice. No extra stroke is counted for the new throw. ",
    image: "profile.jpg",
  },
  {
    name: "Cancel Shot",
    usage: "After shot",
    description:
      "Cancel any shot just taken by an opponent. That opponent must throw again using a different disc of his/her choice. No extra stroke is counted for the new throw. ",
    image: "profile.jpg",
  },
  {
    name: "Cancel Shot",
    usage: "After shot",
    description:
      "Cancel any shot just taken by an opponent. That opponent must throw again using a different disc of his/her choice. No extra stroke is counted for the new throw. ",
    image: "profile.jpg",
  },
  {
    name: "Cancel Shot",
    usage: "After shot",
    description:
      "Cancel any shot just taken by an opponent. That opponent must throw again using a different disc of his/her choice. No extra stroke is counted for the new throw. ",
    image: "profile.jpg",
  },
  {
    name: "Blind Putt",
    usage: "Before putt",
    description:
      "Force an opponent to take the next putt with his/her eyes closed. ",
    image: "profile.jpg",
  },
  {
    name: "Blind Putt",
    usage: "Before putt",
    description:
      "Force an opponent to take the next putt with his/her eyes closed. ",
    image: "profile.jpg",
  },
  {
    name: "One Disc Hole",
    usage: "Before tee",
    description:
      "Force an opponent to play the upcoming hole with only 1 of his/her discs (your choice). ",
    image: "profile.jpg",
  },
  {
    name: "One Disc Hole",
    usage: "Before tee",
    description:
      "Force an opponent to play the upcoming hole with only 1 of his/her discs (your choice). ",
    image: "profile.jpg",
  },
  {
    name: "Ban a disc",
    usage: "Before tee",
    description:
      "Choose an opponent's disc. That disc can not be used for the remainder of the round.",
    image: "profile.jpg",
  },
  {
    name: "Bad Lie",
    usage: "Before shot",
    description:
      "Reposition an opponent's lie by 10 paces (30 feet) in any direction, provided it will not be out of bounds.",
    image: "profile.jpg",
  },
  {
    name: "Lie Swap",
    usage: "After shot",
    description:
      "After all players have taken thier tee shots, trade lies with an opponent of your choice. ",
    image: "profile.jpg",
  },
  {
    name: "Lie Swap",
    usage: "After shot",
    description:
      "After all players have taken thier tee shots, trade lies with an opponent of your choice. ",
    image: "profile.jpg",
  },
  {
    name: "Ambidextrous Drive",
    usage: "Before shot",
    description:
      "Force an opponent to take the upcoming drive with his/her off hand. If right handed he/she must drive left-handed and vice versa.",
    image: "profile.jpg",
  },
  {
    name: "Ambidextrous Drive",
    usage: "Before shot",
    description:
      "Force an opponent to take the upcoming drive with his/her off hand. If right handed he/she must drive left-handed and vice versa.",
    image: "profile.jpg",
  },
  {
    name: "Create Tee Solo",
    usage: "Before tee",
    description:
      "Use 2 of your discs to create a new tee for an opponent on the upcoming drive. The new tee placement must be within 10 meters of the original tee.",
    image: "profile.jpg",
  },
  {
    name: "Create Tee Solo",
    usage: "Before tee",
    description:
      "Use 2 of your discs to create a new tee for an opponent on the upcoming drive. The new tee placement must be within 10 meters of the original tee.",
    image: "profile.jpg",
  },
  {
    name: "Bad Lie",
    usage: "Before shot",
    description:
      "Reposition an opponent's lie by 10 meters in any direction, provided it will not be out of bounds.",
    image: "profile.jpg",
  },
  {
    name: "Bad Lie",
    usage: "Before shot",
    description:
      "Reposition an opponent's lie by 10 meters in any direction, provided it will not be out of bounds.",
    image: "profile.jpg",
  },
  {
    name: "Backhand",
    usage: "Before shot",
    description:
      "Force an opponent to throw a backhand on the upcoming drive or approach",
    image: "profile.jpg",
  },
  {
    name: "Backhand",
    usage: "Before shot",
    description:
      "Force an opponent to throw a backhand on the upcoming drive or approach",
    image: "profile.jpg",
  },
  {
    name: "Rubber and Glue",
    usage: "After card",
    description:
      "If an opponent just played a card that targets only you, force that opponent to carry out the instructions on the card instead of you. ",
    image: "profile.jpg",
  },
  {
    name: "Rubber and Glue",
    usage: "After card",
    description:
      "If an opponent just played a card that targets only you, force that opponent to carry out the instructions on the card instead of you. ",
    image: "profile.jpg",
  },
  {
    name: "No Way",
    usage: "After card",
    description: "Cancel any card just played.",
    image: "profile.jpg",
  },
  {
    name: "No Way",
    usage: "After card",
    description: "Cancel any card just played.",
    image: "profile.jpg",
  },
  {
    name: "No Way",
    usage: "After card",
    description: "Cancel any card just played.",
    image: "profile.jpg",
  },
  {
    name: "No Way",
    usage: "After card",
    description: "Cancel any card just played.",
    image: "profile.jpg",
  },
  {
    name: "New is always better",
    usage: "Before tee",
    description: "Discard your hand and draw 5 new card",
    image: "profile.jpg",
  },
  {
    name: "New is always better",
    usage: "Before tee",
    description: "Discard your hand and draw 5 new card",
    image: "profile.jpg",
  },
  {
    name: "Nutmeg",
    usage: "Before shot",
    description:
      "Force an opponent to throw the next throw between his/her legs.",
    image: "profile.jpg",
  },
  {
    name: "Twice or its luck",
    usage: "Before tee",
    description: "Force an opponen to play next hole as tough shot",
    image: "profile.jpg",
  },
  {
    name: "Double hands",
    usage: "Before shot",
    description:
      "Force an opponen to throw the next throw using both hands. (Both hands will release the disc at the same time)",
    image: "profile.jpg",
  },
];
