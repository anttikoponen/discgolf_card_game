// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { PrismaClient } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";

type Card = {
  id: number;
  name: string;
  usage: string;
  description: string;
  image: string;
  createdAt: Date;
};

const prisma = new PrismaClient();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Card | Card[] | { message: string; error: string }>
) {
  try {
    // Process a GET request
    switch (req.method) {
      case "GET":
        const cards = await prisma.card.findMany();
        res.status(200).json(cards);

        break;

      case "POST":
        const newCard = await prisma.card.create({
          data: {
            name: req.body?.name,
            usage: req.body?.usage,
            description: req.body?.description,
            image: req.body?.image,
          },
        });
        res.status(200).json(newCard);
        break;
      // case "DELETE":

      default:
        res
          .status(404)
          .json({ message: "Unsupported method", error: "Not found" });
    }
  } catch (error) {
    const e = error as any;
    res.status(500).json({ message: "Ops.. error", error: e?.message });
  }
}
