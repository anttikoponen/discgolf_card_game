export type CardType = {
  id: number
  name: string
  usage: string
  description: string
  image: string
  createdAt: string
}
