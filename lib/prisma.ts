import { PrismaClient } from "@prisma/client"
import { CardType } from "../types/types"

const globalForPrisma = global as unknown as { prisma: PrismaClient }

export const prisma =
  globalForPrisma.prisma ||
  new PrismaClient({
    log: ["query"],
  })
if (process.env.NODE_ENV !== "production") globalForPrisma.prisma

export const getCards = async (): Promise<CardType[]> => {
  const cards = await prisma.card.findMany()
  return cards.map(card => ({
    id: card.id,
    name: card.name,
    usage: card.usage,
    description: card.description,
    image: card.image,
    createdAt: card.createdAt.toISOString(),
  }))
}
